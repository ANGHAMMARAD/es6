import { randomNumber } from './helpers/random'

export function fight(firstFighter, secondFighter) {
  let first_health = firstFighter.health;
  let second_health = secondFighter.health;
  let order = (randomNumber(0,1)>=0.5);
  console.log(order);
  while(first_health>0.0 && second_health>0.0){
    console.log(firstFighter.name+" "+first_health+" / "+secondFighter.name+" "+second_health);
    if(order){
      second_health-=getDamage(firstFighter, secondFighter);
      if(second_health>0.0) first_health-=getDamage(secondFighter, firstFighter);
    }
    else{
      first_health-=getDamage(secondFighter, firstFighter);
      if(first_health>0.0) second_health-=getDamage(firstFighter, secondFighter);
    }
  }
  console.log(firstFighter.name+" "+first_health+" / "+secondFighter.name+" "+second_health);
  return first_health > 0.0 ? firstFighter : secondFighter;
}

export function getDamage(attacker, enemy) {
  let damage = getHitPower(attacker)-getBlockPower(enemy);
  console.log("Damage "+damage);
  return Math.max(damage, 0);
}

export function getHitPower(fighter) {
  return fighter.attack*randomNumber(1,2);
}

export function getBlockPower(fighter) {
  return fighter.defense*randomNumber(1,2);
}