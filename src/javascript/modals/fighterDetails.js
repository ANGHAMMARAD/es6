import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name , attack, defense, health, source} = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const fighterTextDetails = createElement({ tagName: 'div', className: '' });
  const nameElement = createElement({ tagName: 'p', className: 'fighter-detail' });
  nameElement.innerText = 'Name: '+name;
  const attackElement = createElement({ tagName: 'p', className: 'fighter-detail' });
  attackElement.innerText = 'Attack: '+attack;
  const defenseElement = createElement({ tagName: 'p', className: 'fighter-detail' });
  defenseElement.innerText = 'Defense: '+defense;
  const healthElement = createElement({ tagName: 'p', className: 'fighter-detail' });
  healthElement.innerText = 'Health: '+health;

  const attributes = { src: source };
  const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });

  fighterTextDetails.append(nameElement);
  fighterTextDetails.append(attackElement);
  fighterTextDetails.append(defenseElement);
  fighterTextDetails.append(healthElement);
  fighterDetails.append(fighterTextDetails);
  fighterDetails.append(imgElement);
  // show fighter name, attack, defense, health, image
  return fighterDetails;
}
