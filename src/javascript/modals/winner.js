import { showModal } from './modal';
import { createElement } from '../helpers/domHelper';
export  function showWinnerModal(fighter) {
  const title = 'Winner';
  const bodyElement = createElement({tagName:"div", className: "winner-body"})
  const nameElement = createElement({ tagName: 'p', className: 'winner-detail' });
  nameElement.innerText = fighter.name;
  const attributes = { src: fighter.source };
  const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });
  bodyElement.append(nameElement);
  bodyElement.append(imgElement);
  showModal({ title, bodyElement });
}